//
//  OpenCVWrapper.m
//  AimDetector
//
//  Created by max kryuchkov on 12/5/18.
//  Copyright © 2018 max kryuchkov. All rights reserved.
//

#import "OpenCVWrapper.h"
#import "CircleData.h"

//#import "cppDetector/searchTarget.cpp"
#import "AimFinder/AimType.hpp"
#include "AimFinder.hpp"

@implementation OpenCVWrapper

+(AimData*)searchAim: (UIImage*) image {
    cv::Mat matrix = [self cvMatFromUIImage: image];
    
    BaseAim *aimModel = new IpscAim();
    DetectedData detectedData = AimFinder::aimFinderMain(matrix, aimModel);
    
    NSMutableArray *result = [NSMutableArray array];
    for (int i = 0; i < detectedData.aimContour.size(); i++) {
        Contour item = detectedData.aimContour.at(i);
        NSMutableArray *coordinateArray = [NSMutableArray array];
        for (int j = 0; j < item.size(); j++) {
            auto contourPoint = item.at(j);
            AimPoint* objcContourPoint = [[AimPoint alloc] init: contourPoint.x _y: contourPoint.y];
            [coordinateArray addObject: objcContourPoint];
        }
        [result addObject: coordinateArray];
    }
    
    NSMutableArray* hitsArray = [NSMutableArray array];
    hitsArray = [NSMutableArray array];
    for (int j = 0; j < detectedData.aimHits.size(); j++) {
        auto p = detectedData.aimHits.at(j);
        AimPoint* hitPoint = [[AimPoint alloc] init: (int)p.x _y: (int)p.y];
        [hitsArray addObject: hitPoint];
    }
    
    UIImage* resImg = [self UIImageFromCVMat: matrix];
    NSString* resultsString = [NSString stringWithFormat:@"%s", detectedData.aimResults.c_str()];
    AimData* resultData = [[AimData alloc] init: result _hits: hitsArray _img: resImg _hitResult: resultsString];
    
    return resultData;
    
    /*
    AbstractAim abstractAim;
    vector<Point2f> hitsVector;
    vector<vector<Contour>> circles;
    Point2f aimCenter;
    ContourList v;
    cv::Mat resultMatrix = Demo::searchTarget(matrix, abstractAim, hitsVector, circles, aimCenter, v);
    */
    /*
    if (circles.size() > 0) {
       
        
        for (int i = 0; i < v.size(); i++) {
            NSMutableArray *coords = [NSMutableArray array];
            auto item = v.at(i);
            for (int j = 0; j < item.size(); j++) {
                cv::Point cd = item.at(j);
                AimPoint* cp = [[AimPoint alloc] init: cd.x _y: cd.y];
                [coords addObject: cp];
            }
            [result addObject: coords];
        }
     
      
        for (int k = 0; k < circles.size(); k++) {
            auto circleGroup = circles.at(k);
            NSMutableArray *objcGroup = [NSMutableArray array];
            for (int i = 0; i < circleGroup.size(); i++) {
                Contour groupItem = circleGroup.at(i);
                NSMutableArray *coordinateArray = [NSMutableArray array];
                for (int j = 0; j < groupItem.size(); j++) {
                    auto contourPoint = groupItem.at(j);
                    AimPoint* objcContourPoint = [[AimPoint alloc] init: contourPoint.x _y: contourPoint.y];
                    [coordinateArray addObject: objcContourPoint];
                }
                [objcGroup addObject: coordinateArray];
            }
            [result addObject: objcGroup];
        }
        
        hitsArray = [NSMutableArray array];
        for (int j = 0; j < hitsVector.size(); j++) {
            auto p = hitsVector.at(j);
            AimPoint* hitPoint = [[AimPoint alloc] init: (int)p.x _y: (int)p.y];
            [hitsArray addObject: hitPoint];
        }
    }
    //cout << "(" << aimCenter.x << ", " << aimCenter.y << ")\n";
    AimPoint* centerPoint = [[AimPoint alloc] init: (int)aimCenter.x _y: (int)aimCenter.y];
    UIImage* resImg = [self UIImageFromCVMat: matrix];
    AimData* resultData = [[AimData alloc] init: result _hits: hitsArray _img: resImg];
    return resultData; */
}

+(cv::Mat)cvMatFromUIImage:(UIImage *)image {
    CGColorSpaceRef colorSpace = CGImageGetColorSpace(image.CGImage);
    CGFloat cols = image.size.width;
    CGFloat rows = image.size.height;
    
    cv::Mat cvMat(rows, cols, CV_8UC4); // 8 bits per component, 4 channels (color channels + alpha)
    
    CGContextRef contextRef = CGBitmapContextCreate(cvMat.data,                 // Pointer to  data
                                                    cols,                       // Width of bitmap
                                                    rows,                       // Height of bitmap
                                                    8,                          // Bits per component
                                                    cvMat.step[0],              // Bytes per row
                                                    colorSpace,                 // Colorspace
                                                    kCGImageAlphaNoneSkipLast |
                                                    kCGBitmapByteOrderDefault); // Bitmap info flags
    
    CGContextDrawImage(contextRef, CGRectMake(0, 0, cols, rows), image.CGImage);
    CGContextRelease(contextRef);
    
    return cvMat;
}

+(UIImage *)UIImageFromCVMat:(cv::Mat)cvMat {
    NSData *data = [NSData dataWithBytes:cvMat.data length:cvMat.elemSize()*cvMat.total()];
    CGColorSpaceRef colorSpace;
    
    if (cvMat.elemSize() == 1) {
        colorSpace = CGColorSpaceCreateDeviceGray();
    } else {
        colorSpace = CGColorSpaceCreateDeviceRGB();
    }
    
    CGDataProviderRef provider = CGDataProviderCreateWithCFData((__bridge CFDataRef)data);
    
    // Creating CGImage from cv::Mat
    CGImageRef imageRef = CGImageCreate(cvMat.cols,                                 //width
                                        cvMat.rows,                                 //height
                                        8,                                          //bits per component
                                        8 * cvMat.elemSize(),                       //bits per pixel
                                        cvMat.step[0],                            //bytesPerRow
                                        colorSpace,                                 //colorspace
                                        kCGImageAlphaNoneSkipLast|kCGBitmapByteOrderDefault,// bitmap info
                                        provider,                                   //CGDataProviderRef
                                        NULL,                                       //decode
                                        false,                                      //should interpolate
                                        kCGRenderingIntentDefault                   //intent
                                        );
    
    
    // Getting UIImage from CGImage
    UIImage *finalImage = [UIImage imageWithCGImage:imageRef scale: 1.0 orientation: UIImageOrientationRight];
    CGImageRelease(imageRef);
    CGDataProviderRelease(provider);
    CGColorSpaceRelease(colorSpace);
    
    return finalImage;
}
@end
