//
//  CircleData.h
//  AimDetector
//
//  Created by max kruchkov on 11/8/18.
//  Copyright © 2018 max kruchkov. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface CircleData: NSObject
    -(id)init: (int)_x _y:(int)_y _r:(int)_r;
    -(void)dealloc;

    @property (nonatomic) int r;
    @property (nonatomic) int x;
    @property (nonatomic) int y;
@end

NS_ASSUME_NONNULL_END
