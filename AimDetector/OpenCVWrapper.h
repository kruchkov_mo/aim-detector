//
//  OpenCVWrapper.h
//  AimDetector
//
//  Created by max kryuchkov on 12/5/18.
//  Copyright © 2018 max kryuchkov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import "AimData.h"
#import "AimPoint.h"

NS_ASSUME_NONNULL_BEGIN

@interface OpenCVWrapper : NSObject
    +(AimData *)searchAim: (UIImage*) image;
@end

NS_ASSUME_NONNULL_END
