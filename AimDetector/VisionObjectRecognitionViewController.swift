//
//  VisionObjectRecognitionViewController.swift
//  AimDetector
//
//  Created by max kryuchkov on 12/3/18.
//  Copyright © 2018 max kryuchkov. All rights reserved.
//

import UIKit
import AVFoundation
import Vision

class VisionObjectRecognitionViewController: ViewController {
    
    struct HitData {
        var i: Int
        var p: AimPoint
    }
    
    var hitsTracking: [HitData] = []
    var period = 0
    var periodStr = ""
    private func clearHits(arr: [HitData]) -> [HitData] {
        var result: [HitData] = []
        for i in 0 ..< arr.count {
            if arr[i].i > 0 {
                result.append(arr[i])
            }
        }
        return result
    }
    
    private func cmp(a: AimPoint, b: AimPoint) -> Bool {
        let eps = 10.0
        return fabs(Double(a.x - b.x)) <= eps && fabs(Double(a.y - b.y)) <= eps
    }
    private func contains(arr: [HitData], p: AimPoint) -> Int? {
        for i in 0 ..< arr.count {
            let a = arr[i].p
            if cmp(a: a, b: p) {
                return i
            }
        }
        return nil
    }
    
    private var detectionOverlay: CALayer! = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.screenTapped))
        self.view.addGestureRecognizer(tap)
        side = min(view.frame.width, view.frame.height) - 2.0*padding
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        startCaptureSession()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        stopCaptureSession()
    }
    
    var side: CGFloat! {
        didSet {
            print(side)
        }
    }
    let padding: CGFloat = 10
    
    func clearLayer() {
        CATransaction.begin()
        CATransaction.setValue(kCFBooleanTrue, forKey: kCATransactionDisableActions)
        detectionOverlay.sublayers = nil
        self.updateLayerGeometry()
        CATransaction.commit()
    }
    
    func drawVisionRequestResults(aimData: AimData) {
        
        
        CATransaction.begin()
        CATransaction.setValue(kCFBooleanTrue, forKey: kCATransactionDisableActions)
        detectionOverlay.sublayers = nil
        
        let frameLayer = createSquareLayer()
        detectionOverlay.addSublayer(frameLayer)
        if let aimContours = aimData.aim as? [[AimPoint]],
            aimContours.count > 0 {
            let detectedAimLayer = createAimLayer(contours: aimContours)
            detectionOverlay.addSublayer(detectedAimLayer)
            
            if let hits = aimData.hits as? [AimPoint] {
                if hits.count == 0 {
                    hitsTracking = []
                }
                for hit in hits {
                    if contains(arr: hitsTracking, p: hit) == nil {
                        let t = HitData(i: 1, p: hit)
                        hitsTracking.append(t)
                    }
                }
                let detectedAimHitsLayer = createAimHitsLayer()
                detectionOverlay.addSublayer(detectedAimHitsLayer)
                
                hitsTracking = clearHits(arr: hitsTracking)
            }
            period = (period + 1) % 3
            if period == 0 {
                periodStr = aimData.hitResult
            }
            let textLayer = createTextLayer(periodStr)
            detectionOverlay.addSublayer(textLayer)
        }
        
        /*
        let l = createImageLayer(aimData.resultImage)
        detectionOverlay.addSublayer(l)
        */
        
        self.updateLayerGeometry()
        CATransaction.commit()
    }
    
    override func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        
        guard let pixelBuffer = CMSampleBufferGetImageBuffer(sampleBuffer) else {
            return
        }
        //let exifOrientation = exifOrientationFromDeviceOrientation()
      
        let pixelImage = convert(cmage: CIImage(cvImageBuffer: pixelBuffer))
        let croppedImage = self.cropToBounds(image: pixelImage)
    
        let aimData = OpenCVWrapper.searchAim(croppedImage)
        drawVisionRequestResults(aimData: aimData)
    }
 
    override func setupAVCapture() {
        super.setupAVCapture()
        
        // setup Vision parts
        setupLayers()
        updateLayerGeometry()
        //setupVision()
        
        // start the capture (called in viewWillAppear)
        //startCaptureSession()
    }
    
    func setupLayers() {
        detectionOverlay = CALayer() // container layer that has all the renderings of the observations
        detectionOverlay.name = "DetectionOverlay"
        detectionOverlay.bounds = CGRect(x: 0.0,
                                         y: 0.0,
                                         width: bufferSize.width,
                                         height: bufferSize.height)
        detectionOverlay.position = CGPoint(x: rootLayer.bounds.midX, y: rootLayer.bounds.midY)
        rootLayer.addSublayer(detectionOverlay)
    }
    
    func updateLayerGeometry() {
        let bounds = rootLayer.bounds
        var scale: CGFloat
        
        let xScale: CGFloat = bounds.size.width / bufferSize.height
        let yScale: CGFloat = bounds.size.height / bufferSize.width
        
        scale = fmax(xScale, yScale)
        if scale.isInfinite {
            scale = 1.0
        }
        CATransaction.begin()
        CATransaction.setValue(kCFBooleanTrue, forKey: kCATransactionDisableActions)
        
        // rotate the layer into screen orientation and scale and mirror
        detectionOverlay.setAffineTransform(CGAffineTransform(rotationAngle: CGFloat(.pi / 2.0)).scaledBy(x: scale, y: -scale))
        // center the layer
        detectionOverlay.position = CGPoint (x: bounds.midX, y: bounds.midY)
        
        CATransaction.commit()
        
    }
    
    func createTextSubLayerInBounds(_ bounds: CGRect, identifier: String, confidence: VNConfidence) -> CATextLayer {
        let textLayer = CATextLayer()
        textLayer.name = "Object Label"
        let formattedString = NSMutableAttributedString(string: String(format: "\(identifier)", confidence))
        let largeFont = UIFont(name: "Helvetica", size: 24.0)!
        
        formattedString.addAttributes([NSAttributedString.Key.font: largeFont], range: NSRange(location: 0, length: identifier.count))
        textLayer.string = formattedString
        textLayer.bounds = CGRect(x: 0, y: 0, width: bounds.size.height - 10, height: bounds.size.width - 10)
        textLayer.position = CGPoint(x: bounds.midX, y: bounds.midY)
        //textLayer.position = CGPoint(x: self.view.bounds.midX, y: self.view.bounds.midY)
        textLayer.shadowOpacity = 0.7
        textLayer.shadowOffset = CGSize(width: 2, height: 2)
        
        textLayer.foregroundColor = CGColor(colorSpace: CGColorSpaceCreateDeviceRGB(), components: [0.95, 0.7, 0.2, 0.3])
        textLayer.backgroundColor = UIColor.clear.cgColor
        textLayer.contentsScale = 2.0 // retina rendering
        // rotate the layer into screen orientation and scale and mirror
        textLayer.setAffineTransform(CGAffineTransform(rotationAngle: CGFloat(.pi / 2.0)).scaledBy(x: 1.0, y: -1.0))
        return textLayer
    }
    
    func createRoundedRectLayerWithBounds(_ bounds: CGRect) -> CALayer {
        let shapeLayer = CALayer()
        shapeLayer.bounds = bounds
        shapeLayer.position = CGPoint(x: bounds.midX, y: bounds.midY)
        shapeLayer.name = "Found Object"
        shapeLayer.backgroundColor = CGColor(colorSpace: CGColorSpaceCreateDeviceRGB(), components: [0.95, 0.7, 0.2, 0.3])
        shapeLayer.cornerRadius = 7
        return shapeLayer
    }
    
    func createSquareLayer() -> CALayer {
        //let side: CGFloat = min(view.bounds.width, view.bounds.height) - 2.0*padding
        let shapeLayer = CALayer()
        
        shapeLayer.bounds = CGRect(x: 0.0, y: 0.0, width: side, height: side)
        shapeLayer.position = CGPoint(x: detectionOverlay.bounds.midX, y: detectionOverlay.bounds.midY)
        //print("side: \(side)\npos: \(shapeLayer.position)\nview frame: \(view.bounds)")
        shapeLayer.name = "Square frame"
        shapeLayer.borderColor = CGColor(colorSpace: CGColorSpaceCreateDeviceRGB(), components: [0.95, 0.7, 0.2, 0.45])
        shapeLayer.borderWidth = 3.0
        return shapeLayer
    }
    
    func createAimCenterLayer(point: AimPoint) -> CALayer {
        
        let shapeLayer = CAShapeLayer()
        shapeLayer.bounds = CGRect(x: 0.0, y: 0.0, width: side, height: side)
        shapeLayer.position = CGPoint(x: detectionOverlay.bounds.midX, y: detectionOverlay.bounds.midY)
        shapeLayer.name = "Aim center"
        
        let centerShape = UIBezierPath(
            arcCenter: CGPoint(x: CGFloat(point.x), y: CGFloat(point.y)),   //CGPoint(x: Int(point.x), y: Int(point.y)),
            radius: 5,
            startAngle: 0.0,
            endAngle: CGFloat(2.0 * .pi),
            clockwise: true)
    
        shapeLayer.path = centerShape.cgPath
        
        shapeLayer.fillColor = UIColor(red: 0.0, green: 1.0, blue: 0.1, alpha: 0.3).cgColor
        shapeLayer.strokeColor = UIColor(red: 0.1, green: 1.0, blue: 0.2, alpha: 0.5).cgColor
        
        return shapeLayer
    }
    
    func createAimCirclesLayer(circles: [[CircleData]], center: AimPoint) -> CALayer {
        let shapeLayer = CAShapeLayer()
        
        shapeLayer.bounds = CGRect(x: 0.0, y: 0.0, width: side, height: side)
        shapeLayer.position = CGPoint(x: detectionOverlay.bounds.midX, y: detectionOverlay.bounds.midY)
        shapeLayer.name = "Detected aim"
        
        let bezierPath = UIBezierPath()
        for cc in circles {
            for c in cc {
                
                let currentCircle = UIBezierPath(arcCenter: CGPoint(x: CGFloat(c.x), y: CGFloat(c.y)), radius: CGFloat(c.r), startAngle: 0.0, endAngle: CGFloat(2.0 * .pi), clockwise: true)
                UIColor.randomColor().setStroke()
                bezierPath.lineWidth = 2
                bezierPath.stroke()
                bezierPath.append(currentCircle)
            }
        }
        
        shapeLayer.path = bezierPath.cgPath
        shapeLayer.fillColor = nil
        shapeLayer.strokeColor = UIColor(red: 1.0, green: 0.2, blue: 0.0, alpha: 0.6).cgColor
        shapeLayer.lineWidth = 2.0
        
        return shapeLayer
    }
    
    func createAimLayer(contours: [[AimPoint]]) -> CALayer {
        
        let shapeLayer = CAShapeLayer()
        
        shapeLayer.bounds = CGRect(x: 0.0, y: 0.0, width: side, height: side)
        shapeLayer.position = CGPoint(x: detectionOverlay.bounds.midX, y: detectionOverlay.bounds.midY)
        shapeLayer.name = "Detected aim"
        let sumPath = UIBezierPath()
     
        for contour in contours {
            //let currentColor = UIColor.randomColor()
            let p = UIBezierPath()
            let startPoint = CGPoint(x: CGFloat(contour[0].x), y: CGFloat(contour[0].y))
            p.move(to: startPoint)
            for i in 1 ..< contour.count {
                p.addLine(to: CGPoint(x: CGFloat(contour[i].x), y: CGFloat(contour[i].y)))
            }
            p.addLine(to: startPoint)
            sumPath.append(p)
        }
        shapeLayer.path = sumPath.cgPath
        shapeLayer.fillColor = nil
        shapeLayer.strokeColor = UIColor(red: 1.0, green: 0.0, blue: 0.0, alpha: 0.5).cgColor
        shapeLayer.lineWidth = 2.0
        
        return shapeLayer
    }
    
    /*
    func createAimLayer(contours: [[AimPoint]]) -> CALayer {
        
        let shapeLayer = CAShapeLayer()
        
        shapeLayer.bounds = CGRect(x: 0.0, y: 0.0, width: side, height: side)
        shapeLayer.position = CGPoint(x: detectionOverlay.bounds.midX, y: detectionOverlay.bounds.midY)
        shapeLayer.name = "Detected aim"
        let p = UIBezierPath()
    
        for contour in contours {
            if contour.count < 2 {
                continue
            }
            let startPoint = CGPoint(x: CGFloat(contour[0].x), y: CGFloat(contour[0].y))
            p.move(to: startPoint)
            for i in 1 ..< contour.count {
                p.addLine(to: CGPoint(x: CGFloat(contour[i].x), y: CGFloat(contour[i].y)))
            }
            p.addLine(to: startPoint)
        }
        
        shapeLayer.path = p.cgPath
        shapeLayer.fillColor = nil
        shapeLayer.strokeColor = UIColor(red: 1.0, green: 0.0, blue: 0.0, alpha: 0.5).cgColor
        shapeLayer.lineWidth = 2.0
        
        return shapeLayer
    }*/
    
    func createAimHitsLayer(/*hits: [AimPoint]*/) -> CALayer {
        let shapeLayer = CAShapeLayer()
        
        shapeLayer.bounds = CGRect(x: 0.0, y: 0.0, width: side, height: side)
        shapeLayer.position = CGPoint(x: detectionOverlay.bounds.midX, y: detectionOverlay.bounds.midY)
        shapeLayer.name = "Detected aim hits"
        let bezierPath = UIBezierPath()

        for j in 0 ..< hitsTracking.count {
            var hit = hitsTracking[j]
            if hit.i == 0 {
                continue
            }
            hit.i += 1
            let point = CGPoint(x: CGFloat(hit.p.x), y: CGFloat(hit.p.y))
            let currentCircle = UIBezierPath(arcCenter: point, radius: side/70, startAngle: 0.0, endAngle: CGFloat(2.0 * .pi), clockwise: true)
            bezierPath.append(currentCircle)
        }
        shapeLayer.path = bezierPath.cgPath
        shapeLayer.borderColor = UIColor(red: 1.0, green: 1.0, blue: 0.0, alpha: 0.6).cgColor
        shapeLayer.fillColor = UIColor(red: 1.0, green: 1.0, blue: 0.0, alpha: 0.6).cgColor
        shapeLayer.strokeColor = UIColor(red: 1.0, green: 1.0, blue: 0.0, alpha: 0.6).cgColor
        shapeLayer.lineWidth = 2.0
        
        return shapeLayer
    }
    
    func createTextLayer(_ text: String) -> CALayer {
        guard text != "" else {
            return CALayer()
        }
        
        let side: CGFloat = min(view.bounds.width, view.bounds.height) - 2.0*padding
        
        let textLayer = CATextLayer()
        textLayer.frame = CGRect(x: 0.0, y: 0.0, width: side, height: side)
        textLayer.position = CGPoint(x: detectionOverlay.bounds.midX, y: detectionOverlay.bounds.midY)
        
        textLayer.string = text
        textLayer.font = CTFontCreateWithName("Helvetica-Bold" as CFString, 10, nil)
        textLayer.foregroundColor = UIColor.black.cgColor
        textLayer.isWrapped = true
        textLayer.alignmentMode = .left
        textLayer.contentsScale = UIScreen.main.scale
        textLayer.transform = CATransform3DMakeScale(-1, 1, 1)
            
        return textLayer
    }
   
    func createImageLayer(_ image: UIImage) -> CALayer {
        guard let cgImg = image.cgImage else {
            return CALayer()
        }
        
        let side: CGFloat = min(view.bounds.width, view.bounds.height) - 2.0*padding
        let shapeLayer = CALayer()
        
        shapeLayer.bounds = CGRect(x: 0.0, y: 0.0, width: side, height: side)
        shapeLayer.position = CGPoint(x: detectionOverlay.bounds.midX, y: detectionOverlay.bounds.midY)
        
        shapeLayer.contents = cgImg
        shapeLayer.name = "Square frame"
        return shapeLayer
    }
    
}

extension VisionObjectRecognitionViewController {
    
    func cropToBounds(image: UIImage) -> UIImage {
        let cgimage = image.cgImage!
        let contextImage: UIImage = UIImage(cgImage: cgimage)
        
        let rect = CGRect(x: (contextImage.size.width - self.side)/2, y: (contextImage.size.height - self.side)/2, width: self.side, height: self.side)
        
        let imageRef: CGImage = cgimage.cropping(to: rect)!
        let image: UIImage = UIImage(cgImage: imageRef, scale: image.scale, orientation: image.imageOrientation)
        
        return image
    }
    
    func convert(cmage: CIImage) -> UIImage {
        let context:CIContext = CIContext.init(options: nil)
        let cgImage:CGImage = context.createCGImage(cmage, from: cmage.extent)!
        let image:UIImage = UIImage.init(cgImage: cgImage)
        return image
    }
    
    
    @objc func screenTapped(sender: UITapGestureRecognizer? = nil) {
        
        self.side = min(view.bounds.width, view.bounds.height) - 2.0*padding
        
        if let videoConnection = stillImageOutput.connection(with: AVMediaType.video) {
            stillImageOutput.captureStillImageAsynchronously(from: videoConnection) {
                (imageDataSampleBuffer, error) -> Void in
                guard
                    let imageData = AVCaptureStillImageOutput
                        .jpegStillImageNSDataRepresentation(imageDataSampleBuffer!),
                    let im = UIImage(data: imageData) else {
                        print("Cannot capture still image output")
                        return
                }
                
                let finalImage = self.cropToBounds(image: im)
                
                UIImageWriteToSavedPhotosAlbum(finalImage, self, #selector(self.image(_:didFinishSavingWithError:contextInfo:)), nil)
            }
        }
    }
    
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        print("Image saved")
    }
    
    /*
    func buffer(from image: UIImage) -> CVPixelBuffer? {
        let attrs = [kCVPixelBufferCGImageCompatibilityKey: kCFBooleanTrue, kCVPixelBufferCGBitmapContextCompatibilityKey: kCFBooleanTrue] as CFDictionary
        var pixelBuffer : CVPixelBuffer?
        let status = CVPixelBufferCreate(kCFAllocatorDefault, Int(image.size.width), Int(image.size.height), kCVPixelFormatType_32ARGB, attrs, &pixelBuffer)
        guard (status == kCVReturnSuccess) else {
            return nil
        }
        
        CVPixelBufferLockBaseAddress(pixelBuffer!, CVPixelBufferLockFlags(rawValue: 0))
        let pixelData = CVPixelBufferGetBaseAddress(pixelBuffer!)
        
        let rgbColorSpace = CGColorSpaceCreateDeviceRGB()
        let context = CGContext(data: pixelData, width: Int(image.size.width), height: Int(image.size.height), bitsPerComponent: 8, bytesPerRow: CVPixelBufferGetBytesPerRow(pixelBuffer!), space: rgbColorSpace, bitmapInfo: CGImageAlphaInfo.noneSkipFirst.rawValue)
        
        context?.translateBy(x: 0, y: image.size.height)
        context?.scaleBy(x: 1.0, y: -1.0)
        
        UIGraphicsPushContext(context!)
        image.draw(in: CGRect(x: 0, y: 0, width: image.size.width, height: image.size.height))
        UIGraphicsPopContext()
        CVPixelBufferUnlockBaseAddress(pixelBuffer!, CVPixelBufferLockFlags(rawValue: 0))
        
        return pixelBuffer
    }
    */
}
extension UIColor {
    
    class func randomColor(randomAlpha randomApha: Bool = false) -> UIColor{
        
        let redValue = CGFloat(arc4random_uniform(255)) / 255.0;
        let greenValue = CGFloat(arc4random_uniform(255)) / 255.0;
        let blueValue = CGFloat(arc4random_uniform(255)) / 255.0;
        let alphaValue = randomApha ? CGFloat(arc4random_uniform(255)) / 255.0 : 1;
        
        return UIColor(red: redValue, green: greenValue, blue: blueValue, alpha: alphaValue)
        
    }
}
