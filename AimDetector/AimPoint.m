//
//  AimPoint.m
//  AimDetector
//
//  Created by max kruchkov on 11/8/18.
//  Copyright © 2018 max kruchkov. All rights reserved.
//

#import "AimPoint.h"

@implementation AimPoint

-(id)init: (int)c1 _y:(int)c2 {
    self.x = c1;
    self.y = c2;
    return self;
}

-(void)dealloc {
}

@end
