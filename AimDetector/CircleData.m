//
//  CircleData.m
//  AimDetector
//
//  Created by max kruchkov on 11/8/18.
//  Copyright © 2018 max kruchkov. All rights reserved.
//

#import "CircleData.h"

@implementation CircleData

-(id)init: (int)c1 _y:(int)c2 _r:(int)c3 {
    self.x = c1;
    self.y = c2;
    self.r = c3;
    return self;
}

-(void)dealloc {
}

@end
