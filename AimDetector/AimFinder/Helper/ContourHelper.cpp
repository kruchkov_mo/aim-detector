//
//  ContourHelper.cpp
//  OpenCV_test
//
//  Created by max kruchkov on 1/7/19.
//  Copyright © 2019 max kruchkov. All rights reserved.
//

#include "ContourHelper.hpp"

ContourList ContourHelper::scale(const ContourList &list, const float scale) {
    ContourList result;
    if (list.empty()) {
        return result;
    }
    for (int i = 0; i < list.size(); ++i) {
        auto c = list.at(i);
        Contour newContour;
        for (int j = 0; j < c.size(); ++j) {
            Point p = c.at(j);
            int newX = (int) (scale * p.x);
            int newY = (int) (scale * p.y);
            newContour.push_back(Point(newX, newY));
        }
        result.push_back(newContour);
    }
    return result;
}

ContourList ContourHelper::translateContours(const ContourList &list, const Point &offset) {
    ContourList newContourList;
    for (int i = 0; i < list.size(); ++i) {
        auto c = list.at(i);
        Contour newContour;
        for (int j = 0; j < c.size(); ++j) {
            Point newPoint(c.at(j).x + offset.x, c.at(j).y + offset.y);
            newContour.push_back(newPoint);
        }
        newContourList.push_back(newContour);
    }
    return newContourList;
}

Point ContourHelper::getCenter(const Contour &contour) {
    Moments m = moments(contour);
    float cx = m.m10 / m.m00;
    float cy = m.m01 / m.m00;
    return Point(cx, cy);
}
