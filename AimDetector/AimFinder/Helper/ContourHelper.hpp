//
//  ContourHelper.hpp
//  OpenCV_test
//
//  Created by max kruchkov on 1/7/19.
//  Copyright © 2019 max kruchkov. All rights reserved.
//

#ifndef ContourHelper_hpp
#define ContourHelper_hpp

#include "../AimType.hpp"

class ContourHelper {
private:
    explicit ContourHelper() {
    }
public:
    static ContourList scale(const ContourList &list, const float scale = 1.0);
    
    static ContourList translateContours(const ContourList &list, const cv::Point &offset);
    
    static cv::Point getCenter(const Contour &contour);
};

#endif /* ContourHelper_hpp */
