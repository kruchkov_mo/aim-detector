//
//  HitFinder.hpp
//  OpenCV_test
//
//  Created by max kruchkov on 1/7/19.
//  Copyright © 2019 max kruchkov. All rights reserved.
//

#ifndef HitFinder_hpp
#define HitFinder_hpp

#include "../AimModel/BaseAim.hpp"

class HitFinder {
public:
    static vector<Point2f> findHits(Mat &image, const BaseAim &aim, const Contour &mainContour);
    static string evaluateHitScore(const BaseAim &baseAim, const ContourList &list, const vector<Point2f> &hits);
    
private:
    explicit HitFinder() {
    }
    ~HitFinder() {
    }
    
    static bool isNotContainsHit(const vector<Point2f> &hits, const Point2f &hit);
    
    static string evaluateHitScoreIpsc(const ContourList &list, const vector<Point2f> &hits);
    
    static string evaluateHitScoreOlympic(const ContourList &list, const vector<Point2f> &hits);
};

#endif /* HitFinder_hpp */
