//
//  HitFinder.cpp
//  OpenCV_test
//
//  Created by max kruchkov on 1/7/19.
//  Copyright © 2019 max kruchkov. All rights reserved.
//

#include "HitFinder.hpp"

vector<Point2f> HitFinder::findHits(Mat &image, const BaseAim &aim, const Contour &mainContour) {
    // set params
    SimpleBlobDetector::Params params;
    params.filterByArea = 1; // .filterByColor look for dark blobs
    
    params.filterByCircularity = 1;
    params.minCircularity = 0.86;
    params.maxCircularity = 1.0;
    
    params.filterByConvexity = 1;
    params.minConvexity = 0.87;
    
    auto rect = boundingRect(aim.getMain());
    float side = (float) rect.width;
    float minHit = (side / 100.0f) * 1.26f; //1.56f;
    float maxHit = (side / 100.0f) * 2.74f; //2.34f
    params.minArea = 9.2;//PI * pow(minHit/2.0f, 2); //25.0;  //r1*r1*PI; // with minimum 7 pixels
    params.maxArea = 30;//PI * pow(maxHit/2.0f, 2); //110.0;  //r2*r2*PI; // with maximum 20 pixels
    //cout << "r: " << params.minArea << ", " << params.maxArea << "\n";
    
    auto detector = cv::SimpleBlobDetector::create(params);
    std::vector<KeyPoint> keypoints;
    detector->detect(image, keypoints);
    
    vector<Point2f> hits;
    for (int i = 0; i < keypoints.size(); ++i) {
        if (pointPolygonTest(mainContour, keypoints[i].pt, false) > 0) {
            hits.push_back(keypoints[i].pt);
        }
    }
    return hits;
}

string HitFinder::evaluateHitScore(const BaseAim &baseAim, const ContourList &list, const vector<Point2f> &hits) {
    if (hits.empty()) {
        return "";
    }
    switch(baseAim.aimType) {
        case IPSC: return evaluateHitScoreIpsc(list, hits);
        case OLYMPIC: return evaluateHitScoreOlympic(list, hits);
        default: return "";
    }
}

bool HitFinder::isNotContainsHit(const vector<Point2f> &hits, const Point2f &hit) {
    for (int i = 0; i < hits.size(); ++i) {
        if (hits.at(i).x == hit.x && hits.at(i).y == hit.y) {
            return false;
        }
    }
    return true;
}

int random(int min, int max) {
    return min + (rand() % static_cast<int>(max - min + 1));
}

string HitFinder::evaluateHitScoreIpsc(const ContourList &list, const vector<Point2f> &hits) {
    map<string, int> results;
    vector<Point2f> t;
    results["A"] = 0;
    results["B"] = 0;
    results["C"] = 0;
    results["D"] = 0;
    for (int i = 0; i < list.size() - 1; ++i) {
        Contour c = list.at(i);
        for (int j = 0; j < hits.size(); ++j) {
            Point2f hit = hits.at(j);
            if (pointPolygonTest(c, hit, false) > 0 && isNotContainsHit(t, hit)) {
                t.push_back(hit);
                if (i < 2) results["A"] += 1;
                if (i == 2) results["B"] += 1;
                if (i == 3) results["C"] += 1;
                if (i == 4) results["D"] += 1;
            }
        }
    }
    
    string str = "";
    for (auto const &result : results) {
        if (result.second) {
            str.append(result.first + ": " + format("%d; ", result.second));
        }
    }
    return str;
}

string HitFinder::evaluateHitScoreOlympic(const ContourList &list, const vector<Point2f> &hits) {
    const int areaSize = 10;
    int results[areaSize] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    vector<Point2f> t;
    for (int i = 0; i < list.size(); ++i) {
        Contour c = list.at(i);
        for (int j = 0; j < hits.size(); ++j) {
            Point2f hit = hits.at(j);
            if (pointPolygonTest(c, hit, false) > 0 && isNotContainsHit(t, hit)) {
                t.push_back(hit);
                results[(9 - i)] += 1;
            }
        }
    }
    string str = "";
    for (int j = 0; j < areaSize; ++j) {
        if (results[j]) {
            str.append(format("%d: %d; ", j + 1, results[j]));
        }
    }
    return str;
}
