//
//  AimType.hpp
//  OpenCV_test
//
//  Created by max kryuchkov on 1/4/19.
//  Copyright © 2019 max kruchkov. All rights reserved.
//

#ifndef AimType_hpp
#define AimType_hpp

#include <stdio.h>
#include <iostream>
#include <vector>
#include <cmath>
#include <cstring>
#include <map>
#include <opencv2/opencv.hpp>

#define PI 3.14159265

using namespace cv;
using namespace std;

typedef vector<cv::Point> Contour;
typedef vector<Contour> ContourList;

#endif /* AimType_hpp */
