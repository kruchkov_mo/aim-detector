//
//  AimFinder.cpp
//  OpenCV_test
//
//  Created by max kruchkov on 1/7/19.
//  Copyright © 2019 max kruchkov. All rights reserved.
//

#include "AimFinder.hpp"
#include "AimModel/DetectedData.hpp"

DetectedData AimFinder::aimFinderMain(Mat &src, BaseAim *aimModel) {
    DetectedData detectedData;
    if (!src.data) {
        return DetectedData();
    }
    flip(src, src, 0);
    
    // model
    Mat srcGray;
    Scalar c(3, 123, 33);
    
    int thresh = 75;
    cvtColor(src, srcGray, COLOR_BGR2GRAY);
    blur(srcGray, srcGray, Size(3,3));
    
    Mat cannyOutput;
    Canny(srcGray, cannyOutput, thresh, 2 * thresh, 3);
    
    vector<Vec4i> hierarchy;
    ContourList contours;
    findContours(cannyOutput, contours, hierarchy, RETR_TREE, CHAIN_APPROX_SIMPLE);
    ContourList list;
    for (int i = 0; i < contours.size(); ++i) {
        Contour approx;
        Contour contour = contours.at(i);
        approxPolyDP(contour, approx, 0.01 * arcLength(contour, true), true);
        
        //int side = min((int) src.rows, (int) src.cols);
        float sideArea = 200;   //(side / 100.f) * 25.0f;
        if ((approx.size() > 10) && (contourArea(contour) > sideArea)) {
            if (matchShapes(aimModel->getMain(), approx, 1, 0.0) <= 0.05) {
                auto approxBoundingBox = boundingRect(approx);
                auto modelBoundingBox = boundingRect(aimModel->getMain());
                
                auto scaledModel = ContourHelper::scale(aimModel->getContours(), (float) approxBoundingBox.width / (float) modelBoundingBox.width);
                
                auto approxCenter = ContourHelper::getCenter(approx);
                auto modelCenter = ContourHelper::getCenter(scaledModel.at(scaledModel.size() - 1));
                
                Point offset(approxCenter - modelCenter);
                auto translatedContours = ContourHelper::translateContours(scaledModel, offset);
                //drawContours(src, translatedContours, -1, Scalar(1, 250, 250));
                
                detectedData.aimContour = translatedContours;
                // HITS
                auto hits = HitFinder::findHits(src, *aimModel, translatedContours.at(translatedContours.size() - 1));
                auto result = HitFinder::evaluateHitScore(*aimModel, translatedContours, hits);
                detectedData.aimHits = hits;
                detectedData.aimResults = result;
                //putText(src, result, Point(10, 40), FONT_HERSHEY_DUPLEX, 1, Scalar::all(255));
                /*
                for (int i = 0; i < hits.size(); ++i) {
                    auto hit = hits.at(i);
                    if (pointPolygonTest(translatedContours.at(translatedContours.size() - 1), hit, false) > 0) {
                        circle(src, hit, 5.0, Scalar(1, 223, 234), 2);
                    }
                }*/
                break;
            }
        }
    }
    return detectedData;
}
