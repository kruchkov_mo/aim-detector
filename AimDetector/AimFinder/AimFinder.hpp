//
//  AimFinder.hpp
//  OpenCV_test
//
//  Created by max kruchkov on 1/7/19.
//  Copyright © 2019 max kruchkov. All rights reserved.
//

#ifndef AimFinder_hpp
#define AimFinder_hpp

#include "BaseAim.hpp"
#include "HitFinder.hpp"
#include "AimModel/DetectedData.hpp"

class AimFinder {
private:
    explicit AimFinder() {
    }
public:
    static DetectedData aimFinderMain(Mat &src, BaseAim *aimModel);
};

#endif /* AimFinder_hpp */
