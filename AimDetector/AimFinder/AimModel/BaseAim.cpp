//
//  BaseAim.cpp
//  OpenCV_test
//
//  Created by max kryuchkov on 1/4/19.
//  Copyright © 2019 max kruchkov. All rights reserved.
//

#include "BaseAim.hpp"

Rect BaseAim::getBoundingRect() const {
    auto mainContour = this->getMain();
    Rect b = boundingRect(mainContour);
    return Rect(b.br().x - this->padding, b.br().y - this->padding, b.tl().x + this->padding, b.tl().y + this->padding);
}

Contour BaseAim::getMain() const {
    auto n = this->contours.size();
    if (n) {
        return this->contours.at(n - 1);
    }
    return Contour();
}

ContourList BaseAim::getContours() const {
    return this->contours;
}

BaseAim::BaseAim(const int padding) {
    this->aimType = NONE;
    this->padding = padding;
}

BaseAim::~BaseAim() {
}
