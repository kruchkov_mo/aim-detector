//
//  BaseAim.hpp
//  OpenCV_test
//
//  Created by max kryuchkov on 1/4/19.
//  Copyright © 2019 max kruchkov. All rights reserved.
//

#ifndef BaseAim_hpp
#define BaseAim_hpp

#include "../Helper/ContourHelper.hpp"

enum AimType {
    NONE, IPSC, OLYMPIC
};

class BaseAim {
protected:
    ContourList contours;
    
public:
    int padding;
    AimType aimType;
    
    virtual cv::Rect getBoundingRect() const;
    
    virtual Contour getMain() const;
    
    virtual ContourList getContours() const;
    
    explicit BaseAim(const int padding = 0);
    
    virtual ~BaseAim();

    /*
    static Point getCenter(const Contour &contour) {
        Moments m = moments(contour);
        float cx = m.m10 / m.m00;
        float cy = m.m01 / m.m00;
        return Point(cx, cy);
    }
    
    ContourList scale(float scale = 1.0) {
        ContourList result;
        if (contours.empty()) {
            return result;
        }
        for (int i = 0; i < contours.size(); ++i) {
            auto c = contours.at(i);
            Contour newContour;
            for (int j = 0; j < c.size(); ++j) {
                Point p = c.at(j);
                int newX = (int) (scale * p.x);
                int newY = (int) (scale * p.y);
                newContour.push_back(Point(newX, newY));
            }
            result.push_back(newContour);
        }
        return result;
    }
    
    static ContourList translateContours(const ContourList &list, const Point &offset) {
        ContourList newContourList;
        for (int i = 0; i < list.size(); ++i) {
            auto c = list.at(i);
            Contour newContour;
            for (int j = 0; j < c.size(); ++j) {
                Point newPoint(c.at(j).x + offset.x, c.at(j).y + offset.y);
                newContour.push_back(newPoint);
            }
            newContourList.push_back(newContour);
        }
        return newContourList;
    }
    */
    
};


class IpscAim: public BaseAim {
public:
    IpscAim(const int padding = 0): BaseAim(padding) {
        this->aimType = IPSC;
        
        Contour ipscContourD;
        ipscContourD.push_back(cv::Point(30, 50));
        ipscContourD.push_back(cv::Point(100, 50));
        ipscContourD.push_back(cv::Point(110, 60));
        ipscContourD.push_back(cv::Point(110, 140));
        ipscContourD.push_back(cv::Point(95, 170));
        ipscContourD.push_back(cv::Point(35, 170));
        ipscContourD.push_back(cv::Point(20, 140));
        ipscContourD.push_back(cv::Point(20, 60));
        ipscContourD.push_back(cv::Point(30, 50));
        
        Contour ipscContourB;
        ipscContourB.push_back(cv::Point(50, 20));
        ipscContourB.push_back(cv::Point(80, 20));
        ipscContourB.push_back(cv::Point(80, 50));
        ipscContourB.push_back(cv::Point(50, 50));
        ipscContourB.push_back(cv::Point(50, 20));
        
        Contour ipscContourC;
        ipscContourC.push_back(cv::Point(50, 50));
        ipscContourC.push_back(cv::Point(80, 50));
        ipscContourC.push_back(cv::Point(95, 60));
        ipscContourC.push_back(cv::Point(95, 116));
        ipscContourC.push_back(cv::Point(85, 140));
        ipscContourC.push_back(cv::Point(45, 140));
        ipscContourC.push_back(cv::Point(35, 116));
        ipscContourC.push_back(cv::Point(35, 60));
        ipscContourC.push_back(cv::Point(50, 50));
        
        Contour ipscContourA;
        ipscContourA.push_back(cv::Point(50, 60));
        ipscContourA.push_back(cv::Point(80, 60));
        ipscContourA.push_back(cv::Point(80, 116));
        ipscContourA.push_back(cv::Point(50, 116));
        ipscContourA.push_back(cv::Point(50, 60));
        
        Contour ipscContourSmallA;
        ipscContourSmallA.push_back(cv::Point(55, 25));
        ipscContourSmallA.push_back(cv::Point(75, 25));
        ipscContourSmallA.push_back(cv::Point(75, 35));
        ipscContourSmallA.push_back(cv::Point(55, 35));
        ipscContourSmallA.push_back(cv::Point(55, 25));
        
        Contour ipscContourMain;
        ipscContourMain.push_back(cv::Point(29, 49));
        ipscContourMain.push_back(cv::Point(49, 49));
        ipscContourMain.push_back(cv::Point(49, 19));
        ipscContourMain.push_back(cv::Point(81, 19));
        ipscContourMain.push_back(cv::Point(81, 49));
        //ipscContourMain.push_back(cv::Point(96, 49));
        ipscContourMain.push_back(cv::Point(101, 49));
        ipscContourMain.push_back(cv::Point(111, 59));
        ipscContourMain.push_back(cv::Point(111, 141));
        ipscContourMain.push_back(cv::Point(96, 171));
        ipscContourMain.push_back(cv::Point(34, 171));
        ipscContourMain.push_back(cv::Point(19, 140));
        ipscContourMain.push_back(cv::Point(19, 59));
        ipscContourMain.push_back(cv::Point(29, 49));
        
        this->contours.push_back(ipscContourA);
        this->contours.push_back(ipscContourSmallA);
        this->contours.push_back(ipscContourB);
        this->contours.push_back(ipscContourC);
        this->contours.push_back(ipscContourD);
        this->contours.push_back(ipscContourMain);
    }
    
    ~IpscAim() override {
        this->contours.clear();
    }
};


class OlympicAim: public BaseAim {
public:
    OlympicAim(const int padding = 0): BaseAim(padding) {
        this->aimType = OLYMPIC;
        const int circleCount = 10;
        const int minRadius = 20;
        const cv::Point offset(210, 210);
        for (int i = 0; i < circleCount; ++i) {
            Contour currentCircleContour;
            float r = minRadius * (i + 1);
            for (float phi = 0.0f; phi <= 2 * PI; phi += 0.01) {
                cv::Point p(r * sin(phi), r * cos(phi));
                currentCircleContour.push_back(p + offset);
            }
            this->contours.push_back(currentCircleContour);
        }
    }
    
    ~OlympicAim() override {
        this->contours.clear();
    }
};

#endif /* BaseAim_hpp */
