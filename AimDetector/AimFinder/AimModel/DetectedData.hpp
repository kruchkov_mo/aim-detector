//
//  DetectedData.hpp
//  OpenCV_test
//
//  Created by max kryuchkov on 1/8/19.
//  Copyright © 2019 max kruchkov. All rights reserved.
//

#ifndef DetectedData_hpp
#define DetectedData_hpp

#include "../AimType.hpp"

class DetectedData {
private:
public:
    DetectedData() {
        this->aimContour = ContourList();
        this->aimHits = vector<Point2f>();
        this->aimResults = "";
    }
    DetectedData(ContourList contour, vector<Point2f> hits, string results) {
        this->aimContour = contour;
        this->aimHits = hits;
        this->aimResults = results;
    }
    ~DetectedData() {
    }
    ContourList aimContour;
    vector<Point2f> aimHits;
    string aimResults;
};

#endif /* DetectedData_hpp */
