//
//  AimData.h
//  AimDetector
//
//  Created by max kryuchkov on 12/7/18.
//  Copyright © 2018 max kryuchkov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import "AimPoint.h"

NS_ASSUME_NONNULL_BEGIN

@interface AimData : NSObject
    -(id)init: (NSMutableArray*)_aim _hits:(NSMutableArray*)_hits _img:(UIImage*)_img _hitResult:(NSString*)_hitResult;
    -(void)dealloc;

    @property (nonatomic) NSMutableArray* aim;
    @property (nonatomic) NSMutableArray* hits;
    @property (nonatomic) UIImage* resultImage;
    @property (nonatomic) NSString* hitResult;
@end

NS_ASSUME_NONNULL_END
