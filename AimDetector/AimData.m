//
//  AimData.m
//  AimDetector
//
//  Created by max kryuchkov on 12/7/18.
//  Copyright © 2018 max kryuchkov. All rights reserved.
//

#import "AimData.h"

@implementation AimData
    -(id)init: (NSMutableArray*)_aim _hits:(NSMutableArray*)_hits _img:(UIImage*)_img _hitResult:(NSString*)_hitResult {
        self.aim = _aim;
        self.hits = _hits;
        self.resultImage = _img;
        self.hitResult = _hitResult;
        return self;
    }

    -(void)dealloc {
    }
@end
